# README #

# [Live Demo](https://karim3d.itch.io/cheeki-breeki-survival)

Здесь описано с какими проблемами столкнулся и как решал:

* [Мой воркфлоу](https://docs.google.com/document/d/1uovd4xBzRWU1Gm--aobTP1uGkFK_pEegklm0E-dooHw/edit?usp=sharing)

### Недавний апдейт от 11.03 ###
* Переделаны классы отображения лидерборда.
* Подключен виджет историй.

### Запуск ###

1. Запустие сервер \CheekiBreeki Survival\StartServer.bat

Порт к котрому подключается сервер задается в Server.cfg

2. Запустие CheekiBreeki Survival.exe

Порт к которому будет запрашивать доступ клиента, всегда 5000.

### Открытие исходного кода ###

1. Откройте папку CheekiBreeki Survival\Src через Unity Engine 2019.3.0f6

2. Для сервера, откройте CheekiBreeki Survival\Src\Server

### Примечания ###

При запуске bat сервер собирается (если нужно) и запускается в релизной конфигурации (Release).

Чтобы посмотреть код bat файла, откройте при помощи любого текстового редактора.

Уменьшение времени на ответ реализовано, егоможно увидеть в DefaultLogic.cs.

### Используемые технологии ###

1. Unity Engine 2019.3.0f6 (64-bit)

2. [Entity Framework Core](https://docs.microsoft.com/ru-ru/ef/core/) для связи с БД

3. [SQL Lite](https://www.sqlite.org/index.html) в качестве СУБД

4. [ASP.NET Core v2.1](https://docs.microsoft.com/ru-ru/aspnet/core/?view=aspnetcore-2.1) в качестве серера

5. Для работы с JSON, [Unity.JSONUtility](https://docs.unity3d.com/ScriptReference/JsonUtility.html) со своим фиксом (теперь читает массивы)

6. [UnityEngine.Networking](https://www.nuget.org/packages/Unity3D.UnityEngine.Networking/) для формирование и отправки запросов

### Roadmap ###
* Добавить виджет для отображения истории игрока.
* Добавить возможность редактировать порт подключении клиента извне. Ini или json файл.
* Портирование на андроид:
* a. Верстка UI;
* b. Запуск локального сервера.
* Добавление главного меню.
* Добавление нового режима с рандомными числами.



