﻿using System.Collections;
using System.Collections.Generic;
using System;


[Serializable]
public class PlayerSessionReport
{
    public int id;
    public int totalSessionTime;
    public int totalRightAnswer;
}
