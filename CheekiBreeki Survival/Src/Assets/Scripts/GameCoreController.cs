﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*/ Контроллер игровой логики, его единственная задача в том, чтобы
 * 1. Преобразовывать ввод игрока для игровой логики - игрока нажал клавишу.
 * 2. Вызывать события логики - кончилось время на ответ.
 * Наследует монобехэйвор, так как цепляется на GO, чтобы дать кнопкам возможность вызывать функции контроллера.
 */
class GameCoreController : MonoBehaviour, IGameCoreController
{
    GameSettings _settings;
    IGameCore _gameCore;

    private GameCoreController()
    { }

    bool _gameOver = false;
    void IGameCoreController.GameOver(GameOverType type)
    {
        if (_gameOver == false)
        {
            _gameOver = true;
            _gameCore.GameOver(type);
        }
    }

    public void OnClickButtonWithAnswer(string answer)
    {
        _gameCore.SendAnswer(answer);
    }

    public IGameCoreObserverable GetObserverableProvider()
    {
        if (_gameCore == null)
        {
            Debug.LogWarning(string.Format("Get Observerable provider: GameCore is controller {0} is not initialized", gameObject.name));
            return null;
        }
        IGameCoreObserverable provider = _gameCore.GetObserverableProvider();
        return provider;
    }

    public IGameCoreController GetCoreController()
    {
        if(_gameCore == null)
        {
            _gameCore = GameCore.GetCore();
        }
        return this as IGameCoreController; // Для читаемости лучше явно указать преобразование, чем неявно.
    }

    public IGameCoreObserverable GetCoreObserverable()
    {
        if (_gameCore == null)
        {
            _gameCore = GameCore.GetCore();
        }
        IGameCoreObserverable provider = _gameCore.GetObserverableProvider();
        return provider;
    }

    void IGameCoreController.AskNumber(int number)
    {
        _gameCore.AskNumber(number);
    }

    string IGameCoreController.GetRightAnswer(int num)
    {
        return _gameCore.GetResultString(num);
    }

    void IGameCoreController.RightAnswer()
    {
        _gameCore.RightAnswer();
    }

    public void SetPause(bool pause)
    {
        Time.timeScale = pause ? 0 : 1;
    }

    public void AppExit()
    {
        Application.Quit();
    }
}
