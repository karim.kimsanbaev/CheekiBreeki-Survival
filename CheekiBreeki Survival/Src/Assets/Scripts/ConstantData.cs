﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static partial class ConstantDataServer
{
    public const string SERVER_LEADERBOARD_URL = "http://localhost:5000/api/leaderboard";
    public const string SERVER_LEADERBOARD_URL_GET_SORT = "http://localhost:5000/api/leaderboard/sort";
    public static readonly string SERVER_SETTINGS_FILEPATH = Application.streamingAssetsPath + "/ServerSettings.json";
}

public enum GameOverType
{
    EndTime,
    WrongAnswer,
    Win
}
public enum ButtonAnswer
{
    Cheeki,
    Breeki,
    CheekiBreeki,
    Number
}

/// <summary>
/// Настройки для дизайнеров)
/// Так же настраиваются в инспекторе.
/// </summary>
[System.Serializable]
public class GameSettings
{  
    public int StartValue = 1;
    public int EndValue = 100;
    public int Inc = 1;

    public float DelayBetweenAsksInSec = 1;

    public float TimeOnAnswer = 5;
    public float TimeOnAnswerDec = 0.1f;
}
