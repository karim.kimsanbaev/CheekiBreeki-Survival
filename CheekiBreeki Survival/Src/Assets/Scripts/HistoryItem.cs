﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HistoryItem
{
    public int Num { get; private set; }
    public string RightAnswer { get; private set; }
    public string PlayerAnswer { get; private set; }

    public HistoryItem(int num, string rightAnswer)
    {
        Num = num;
        RightAnswer = rightAnswer;
    }

    public void SetPlayerAnswer(string answer)
    {
        PlayerAnswer = answer;
    }
}
