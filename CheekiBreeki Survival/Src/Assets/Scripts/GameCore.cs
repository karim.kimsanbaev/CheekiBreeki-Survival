﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Логика самой игры.
class GameCore : IGameCore, IGameCoreObserverable
{
    private int _num;
    private string _result;
    public int Num
    {
        get
        {
            return _num;
        }
    }
    public string Result
    {
        get
        {
            return _result;
        }
    }

    private static GameCore _instance; // Singleton, игровое ядро может быть только одно.
    private ICheekiBreekiIntsance _cheekiBreeki;

    GameCore()
    { }

    public static IGameCore GetCore()
    {
        if (_instance == null)
        {
            _instance = new GameCore();
        }
        return _instance;
    }

    IGameCoreObserverable IGameCore.GetObserverableProvider()
    {
        return this as IGameCoreObserverable;
    }

    void IGameCore.SendAnswer(string answer)
    {
        ReceivePlayerAnswerEvent(answer);
    }

    public event Action<GameOverType> OnGameOver;
    public event Action<int, string> AskNumberEvent;
    public event Action OnRightAnswer;
    public event Action<string> ReceivePlayerAnswerEvent;
    public event Action OnWrongAnswer;
    public void AnswerNextNumber()
    {
        _num++;

        _cheekiBreeki = CheekiBreeki.Instance;

        _result = _cheekiBreeki.GetResultString(_num);

        AskNumberEvent?.Invoke(_num, _result);
    }

    void IGameCore.RightAnswer()
    {
        OnRightAnswer();
    }
    void IGameCore.WrongAnswer()
    {
        OnWrongAnswer();
    }

    void IGameCore.GameOver(GameOverType type)
    {
        OnGameOver(type);
    }

    string IGameCore.GetResultString(int num)
    {
        _cheekiBreeki = CheekiBreeki.Instance;
        _result = _cheekiBreeki.GetResultString(num);

        return _result;
    }

    void IGameCore.AskNumber(int num)
    {
        _cheekiBreeki = CheekiBreeki.Instance;
        _result = _cheekiBreeki.GetResultString(num);

        AskNumberEvent?.Invoke(num, _result);
    }
}
