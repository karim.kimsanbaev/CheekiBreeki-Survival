﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public static class UITextExtensions
{
    public static void SetText(this Text textComponent, string text, bool active = true)
    {
        textComponent.text = text;
        textComponent.gameObject.SetActive(active);
    }
}
