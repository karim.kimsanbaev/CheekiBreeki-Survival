﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class FileUtility
{
    public static T FromJSONFile<T>(string path)
    {
        string jsonString = File.ReadAllText(path);
        var jsonObject = JsonUtility.FromJson<T>(jsonString);
        return jsonObject;
    }
}
