﻿using System;
using System.Collections;
using System.Collections.Generic;

public class CheekiBreeki : ICheekiBreekiIntsance
{
    private static CheekiBreeki _instance;
    public static CheekiBreeki Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = new CheekiBreeki();
            }
            return _instance;
        }
    }

    public string GetResultString(int num)
    {
        string result = "";

        if(num % 3 == 0)
        {
            result = string.Format("Cheeki");
        }

        if (num % 5 == 0)
        {
            result = string.Format("{0}Breeki", result);
        }

        if (result == "")
        {
            result = num.ToString();
        }

        return result;
    }
}
