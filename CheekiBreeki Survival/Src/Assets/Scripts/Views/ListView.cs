﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ListView<T, TView> : MonoBehaviour, IListView<T, TView> where TView : IItemView<T>
{
    Dictionary<T, TView> _items;
    public Dictionary<T, TView> Items
    {
        get
        {
            if (_items == null)
            {
                _items = new Dictionary<T, TView>();
            }
            return _items;
        }
    }

    [SerializeField] protected TView _sourceView;

    TView IListView<T, TView>.CreateItem(T item, TView sourceView)
    {
        if (Items.ContainsKey(item) == false)
        {
            var go = Instantiate(sourceView.GetGameObject(), transform);
            var view = go.GetComponent<TView>();
            SetSourceData(view, item);
            Items.Add(item, view);
            go.SetActive(true);
            return go.GetComponent<TView>();
        }
        return default;
    }

    int IListView<T, TView>.DestroyItems()
    {
        int countDestroyItems = 0;
        foreach(var item in _items)
        {
            var key = item.Key;
            GameObject go = item.Value.GetGameObject();

            if(go != null && go is GameObject)
            {
                GameObject.DestroyImmediate((go as GameObject));
            }
            countDestroyItems++;
        }
        _items.Clear();
        return countDestroyItems;
    }

    protected abstract void SetSourceData(IItemView<T> view, T item);
}