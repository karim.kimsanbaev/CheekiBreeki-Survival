﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Достаточно закрепить этот скрипт на префабе и отключить скрипт DefaultLogic и числа будут браться случайно.
class RandomNumberLogic : DefaultLogic
{
    protected override IEnumerator AskNumberRange(GameSettings gameSettings)
    {
        int startNum = gameSettings.StartValue;
        int endNum = gameSettings.EndValue;

        float delayBetweenAsks = gameSettings.DelayBetweenAsksInSec;
        float timeOnAnswer = gameSettings.TimeOnAnswer;
        float timeOnAnswerDec = gameSettings.TimeOnAnswerDec;

        var rand = new Random();

        do
        {
            yield return new WaitForSeconds(delayBetweenAsks);

            _num = Random.Range(startNum, endNum);

            AskNumber(_num);

            var tickCoroutine = StartCoroutine(Ticks(timeOnAnswer));

            yield return new WaitUntil(() => _endStep == true);

            StopCoroutine(tickCoroutine);

            if (_gameOver) break;

            timeOnAnswer -= timeOnAnswerDec;

            _endStep = false;
        } while (_gameOver != true);
    }
}
