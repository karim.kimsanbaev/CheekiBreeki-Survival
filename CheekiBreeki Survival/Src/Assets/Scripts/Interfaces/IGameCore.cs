﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Интерфейс, через который контроллер дает команды.
/// </summary>
public interface IGameCore
{
    string GetResultString(int num);
    void AskNumber(int num);
    void SendAnswer(string answer);
    void GameOver(GameOverType type);
    void RightAnswer();
    void WrongAnswer();

    IGameCoreObserverable GetObserverableProvider();
}