﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IItemView<T>
{
    void SetContent(T item);
    GameObject GetGameObject();
}
