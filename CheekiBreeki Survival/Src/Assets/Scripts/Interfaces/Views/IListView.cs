﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IListView<T, TView> where TView : IItemView<T>
{
    Dictionary<T, TView> Items { get; }
    TView CreateItem(T item, TView source);
    int DestroyItems();
}
