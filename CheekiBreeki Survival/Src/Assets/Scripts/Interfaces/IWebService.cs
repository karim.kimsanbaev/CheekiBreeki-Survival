﻿using System.Collections;

public interface IWebService
{
    IEnumerator GetLeaderboard();
    IEnumerator SendSessionReport(PlayerSessionReport report);
    event System.Action<PlayerSessionReport[]> GetLeaderboardCompleted;
}