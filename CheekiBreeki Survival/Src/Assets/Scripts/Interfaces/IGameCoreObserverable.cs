﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Интерфейс, через который игровая логика оповещает наблюдателей о событиях.
/// </summary>
public interface IGameCoreObserverable
{
    event Action<int, string> AskNumberEvent; // Используется Action, чтобы не создавать доп. делегатов, например delegate EventHandler(int number, string rightAnswer).
    event Action<GameOverType> OnGameOver;
    event Action OnRightAnswer;
    event Action OnWrongAnswer;

    event Action<string> ReceivePlayerAnswerEvent;

    int Num { get; }
    string Result { get; }
}