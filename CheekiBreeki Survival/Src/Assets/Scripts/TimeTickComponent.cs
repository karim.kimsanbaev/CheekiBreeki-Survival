﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

interface ITickComponent
{
    float RestTime { get; }
    bool Paused { get; }
    void StartTicks();
    void StopTicks();
    void PauseTicks();
    void ContinueTicks();
    UnityEvent OnEndTime { get; }
}

public class TimeTickComponent : MonoBehaviour, ITickComponent
{
    float _time = 0;
    bool _pause = false;
    float ITickComponent.RestTime => _time;

    bool ITickComponent.Paused => _pause;

    UnityEvent ITickComponent.OnEndTime { get; }

    void ITickComponent.ContinueTicks()
    {
        throw new NotImplementedException();
    }

    void ITickComponent.PauseTicks()
    {
        throw new NotImplementedException();
    }

    void ITickComponent.StartTicks()
    {
        throw new NotImplementedException();
    }

    void ITickComponent.StopTicks()
    {
        throw new NotImplementedException();
    }


    IEnumerator Ticks(float delayInSec)
    {
        yield return new WaitForSeconds(delayInSec);
    }
}
