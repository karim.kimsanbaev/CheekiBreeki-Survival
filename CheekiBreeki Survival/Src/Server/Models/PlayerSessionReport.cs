﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Models
{
    public class PlayerSessionReport
    {
        public long Id { get; set; }
        public int TotalSessionTime { get; set; }
        public int TotalRightAnswer { get; set; }
    }
}
