﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Sqlite;

namespace Server.Models
{
    public class SessionReportContext : DbContext
    {
        public SessionReportContext(DbContextOptions<SessionReportContext> options)
           : base(options)
        {
            this.Database.EnsureCreated();

            UpdateMinAnswersReportId();
        }

        public async void UpdateMinAnswersReportId()
        {
            _count = await PlayerSessionReports.CountAsync();
            if (_count != 0)
            {
                MinAnswersReportId = await GetMinAnswerReportId(PlayerSessionReports);
            }
        }

        private async Task<long> GetMinAnswerReportId(DbSet<PlayerSessionReport> reports)
        {
            var minAnswer = await PlayerSessionReports.MinAsync(r => r.TotalRightAnswer);
            var minAnswersReport = await PlayerSessionReports.FirstOrDefaultAsync(r => r.TotalRightAnswer == minAnswer);
            return minAnswersReport.Id;
        }

        // Всегда храним ссылку на минимальный элемент, чтобы быстро его удалить без поиска.
        public long MinAnswersReportId { get; private set; }
        public DbSet<PlayerSessionReport> PlayerSessionReports { get; set; }

        private int _count = 0;
        public int CountReports
        {
            get
            {
                return _count == 0 ? _count = PlayerSessionReports.Count() : _count;
            }
            private set { _count = value; }
        }
    }
}
