﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Server.Models;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LeaderboardController : ControllerBase
    {
        private readonly SessionReportContext _context;

        public LeaderboardController(SessionReportContext context)
        {
            _context = context;
        }

        // GET: api/leaderoard
        [HttpGet]
        public IEnumerable<PlayerSessionReport> GetPlayerSessionReports()
        {
            return _context.PlayerSessionReports;
        }

        // GET: api/leaderoard/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPlayerSessionReport([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var playerSessionReport = await _context.PlayerSessionReports.FindAsync(id);

            if (playerSessionReport == null)
            {
                return NotFound();
            }

            return Ok(playerSessionReport);
        }

        // GET: api/leaderoard/sort
        [HttpGet("sort")]
        public IActionResult GetSortPlayerSessionReport()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var reports = from r in _context.PlayerSessionReports
                          select r;

            if (reports == null)
            {
                return NotFound();
            }

            reports = reports.OrderByDescending(r => r.TotalRightAnswer);

            return Ok(reports);
        }

        // PUT: api/leaderoard/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlayerSessionReport([FromRoute] long id, [FromBody] PlayerSessionReport playerSessionReport)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != playerSessionReport.Id)
            {
                return BadRequest();
            }

            _context.Entry(playerSessionReport).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlayerSessionReportExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        

        // POST: api/leaderoard
        [HttpPost]
        public async Task<IActionResult> PostPlayerSessionReport([FromBody] PlayerSessionReport playerSessionReport)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_context.CountReports >= ConstantData.MAX_REPORTS_IN_DB)
            {
                await DeletePlayerSessionReport(_context.MinAnswersReportId);
            }
            

            _context.PlayerSessionReports.Add(playerSessionReport);

            bool saveComplete = _context.SaveChangesAsync().IsCompleted;

            if(saveComplete)
                _context.UpdateMinAnswersReportId();

            return CreatedAtAction("GetPlayerSessionReport", new { id = playerSessionReport.Id }, playerSessionReport);
        }

        // DELETE: api/leaderoard/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePlayerSessionReport([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var playerSessionReport = await _context.PlayerSessionReports.FindAsync(id);
            if (playerSessionReport == null)
            {
                return NotFound();
            }

            _context.PlayerSessionReports.Remove(playerSessionReport);
            await _context.SaveChangesAsync();

            return Ok(playerSessionReport);
        }

        private bool PlayerSessionReportExists(long id)
        {
            return _context.PlayerSessionReports.Any(e => e.Id == id);
        }
    }
}