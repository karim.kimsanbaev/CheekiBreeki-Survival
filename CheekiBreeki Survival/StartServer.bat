@ECHO OFF
Rem Set file=Server.cfg

<Server.cfg (for /f "delims=" %%a in ('more') do @set "%%a") 2>nul

dotnet run --project .\Src\Server\Server.csproj --urls="http://localhost:%port%" --configuration Release